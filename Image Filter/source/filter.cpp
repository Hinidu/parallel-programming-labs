#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/core/operations.hpp>

#include <omp.h>

#include "filter.h"

inline
cv::Mat replicate(const size_t n, const cv::Mat &m)
{
    std::vector<cv::Mat> v;
    for (size_t i = 0; i < n; ++i)
        v.push_back(m);
    cv::Mat res;
    cv::merge(v, res);
    return res;
}

cv::Mat filter::apply(const cv::Mat &source) const
{
    cv::Mat kernel = replicate(source.channels(), this->kernel);

    cv::Mat conv_source;
    source.convertTo(conv_source, kernel.type());

    cv::Size size(source.size().width - kernel.size().width, source.size().height - kernel.size().height);
    cv::Mat result(size, source.type());

#pragma omp parallel for default(none)\
    shared(kernel,conv_source,result,size)
    for (int i = 0; i < size.height - kernel.size().height; ++i)
    {
        for(int j = 0; j < size.width - kernel.size().width; ++j)
        {
            cv::Mat submatrix = conv_source(cv::Range(i, i + kernel.size().height), cv::Range(j, j + kernel.size().width));
            cv::Vec4f tmp_sum = sum(submatrix.mul(kernel));
            cv::Vec4f sum = factor * tmp_sum;
            cv::Vec3b pixel(cv::saturate_cast<uchar>(sum[0]),
                    cv::saturate_cast<uchar>(sum[1]),
                    cv::saturate_cast<uchar>(sum[2]));
            result.at<cv::Vec3b>(i, j) = pixel;
        }
    }

    return result;
}
