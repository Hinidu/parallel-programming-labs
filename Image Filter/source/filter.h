#ifndef FILTER_H
#define FILTER_H

struct filter
{
    const char *name;
    cv::Mat kernel;
    float factor;

    template <size_t KERNEL_SIZE>
    filter(const char *name, const float (&kernel)[KERNEL_SIZE][KERNEL_SIZE], const float factor)
    {
        this->name = name;
        this->kernel = cv::Mat(KERNEL_SIZE, KERNEL_SIZE, CV_32FC1, const_cast<float (*)[KERNEL_SIZE]>(kernel));
        this->factor = factor;
    }

    cv::Mat apply(const cv::Mat &source) const;
};

#endif
