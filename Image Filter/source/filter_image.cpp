#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <omp.h>

#include "filter.h"
#include "predefined_filters.h"

int main(int argc, char* argv[])
{
    int number_of_threads = 1;
    const filter* filter = predefined_filters[0];
    const char* filename = NULL;
    const char* resultFilename = "out.jpg";

    char opt;
    while ((opt = getopt(argc, argv, "hlt:f:o:")) != -1)
    {
        switch (opt)
        {
            case 'h':
                printf("usage: %s -t number_of_threads -f filter_name -o result_file file\n", argv[0]);
                printf("       %s -l    -- print all available filters\n", argv[0]);
                printf("       %s -h    -- print this message\n", argv[0]);
                return 0;
            case 'l':
                for (size_t i = 0; predefined_filters[i] != NULL; ++i)
                    puts(predefined_filters[i]->name);
                return 0;
            case 't':
                number_of_threads = strtol(optarg, NULL, 10);
                if (errno == ERANGE || number_of_threads <= 0)
                {
                    fprintf(stderr, "Incorrect number of threads: %s\n", optarg);
                    return 1;
                }
                break;
            case 'f':
                filter = get_filter(optarg);
                if (filter == NULL)
                {
                    fprintf(stderr, "Unknown filter name: %s\n", optarg);
                    return 1;
                }
                break;
            case 'o':
                resultFilename = optarg;
                break;
        }
    }

    if (optind + 1 != argc)
    {
        printf("Wrong arguments format.\nGet additional information about usage with %s -h\n", argv[0]);
        return 1;
    }

    filename = argv[optind];
    if (access(filename, R_OK) == -1)
    {
        printf("Image %s does not exist or does not grant read permissions\n", filename);
        return 2;
    }

    omp_set_num_threads(number_of_threads);

    imwrite(resultFilename, filter->apply(cv::imread(filename)));

    return 0;
}
