#ifndef PREDEFINED_FILTERS_H
#define PREDEFINED_FILTERS_H

const float ID_KERNEL[3][3] =
{
    {0,0,0},
    {0,1,0},
    {0,0,0}
};

filter ID_FILTER("id", ID_KERNEL, 1.0f);

const float BLUR_KERNEL[3][3] =
{
    {0,  0.2,0},
    {0.2,0.2,0.2},
    {0,  0.2,0}
};

filter BLUR_FILTER("blur", BLUR_KERNEL, 1.0f);

const float BLUR_MORE_KERNEL[5][5] =
{
    {0,0,1,0,0},
    {0,1,1,1,0},
    {1,1,1,1,1},
    {0,1,1,1,0},
    {0,0,1,0,0}
};

filter BLUR_MORE_FILTER("blur_more", BLUR_MORE_KERNEL, 1.0f / 13.0f);

const float MOTION_BLUR_KERNEL[9][9] =
{
    {1,0,0,0,0,0,0,0,0},
    {0,1,0,0,0,0,0,0,0},
    {0,0,1,0,0,0,0,0,0},
    {0,0,0,1,0,0,0,0,0},
    {0,0,0,0,1,0,0,0,0},
    {0,0,0,0,0,1,0,0,0},
    {0,0,0,0,0,0,1,0,0},
    {0,0,0,0,0,0,0,1,0},
    {0,0,0,0,0,0,0,0,1}
};

filter MOTION_BLUR_FILTER("motion_blur", MOTION_BLUR_KERNEL, 1.0f / 9.0f);

const float HORIZONTAL_EDGES_KERNEL[5][5] =
{
    {0,0,0,0,0},
    {0,0,0,0,0},
    {-1,-1,2,0,0},
    {0,0,0,0,0},
    {0,0,0,0,0}
};

filter HORIZONTAL_EDGES_FILTER("horizontal_edges", HORIZONTAL_EDGES_KERNEL, 1.0f);

const float VERTICAL_EDGES_KERNEL[5][5] =
{
    {0,0,-1,0,0},
    {0,0,-1,0,0},
    {0,0, 4,0,0},
    {0,0,-1,0,0},
    {0,0,-1,0,0}
};

filter VERTICAL_EDGES_FILTER("vertical_edges", VERTICAL_EDGES_KERNEL, 1.0f);

const float EDGES_45_KERNEL[5][5] =
{
    {-1,0, 0, 0, 0},
    {0,-2, 0, 0, 0},
    {0, 0, 6, 0, 0},
    {0, 0, 0,-2, 0},
    {0, 0, 0, 0,-1}
};

filter EDGES_45_FILTER("edges_45", EDGES_45_KERNEL, 1.0f);

const float ALL_EDGES_KERNEL[3][3] =
{
    {-1, -1, -1},
    {-1,  8, -1},
    {-1, -1, -1}
};

filter ALL_EDGES_FILTER("all_edges", ALL_EDGES_KERNEL, 1.0f);

const float SHARPEN_KERNEL[3][3] =
{
    {-1, -1, -1},
    {-1,  9, -1},
    {-1, -1, -1}
};

filter SHARPEN_FILTER("sharpen", SHARPEN_KERNEL, 1.0f);

const float SUBTLE_SHARPEN_KERNEL[5][5] =
{
    {-1,-1,-1,-1,-1},
    {-1, 2, 2, 2,-1},
    {-1, 2, 8, 2,-1},
    {-1, 2, 2, 2,-1},
    {-1,-1,-1,-1,-1}
};

filter SUBTLE_SHARPEN_FILTER("subtle_sharpen", SUBTLE_SHARPEN_KERNEL, 1.0f / 8.0f);

const float EXCESSIVELY_SHARPEN_KERNEL[3][3] =
{
    {1, 1, 1},
    {1,-7, 1},
    {1, 1, 1}
};

filter EXCESSIVELY_SHARPEN_FILTER("excessively_sharpen", EXCESSIVELY_SHARPEN_KERNEL, 1.0f);

const filter* predefined_filters[] =
{
    &ID_FILTER,
    &BLUR_FILTER,
    &BLUR_MORE_FILTER,
    &MOTION_BLUR_FILTER,
    &HORIZONTAL_EDGES_FILTER,
    &VERTICAL_EDGES_FILTER,
    &EDGES_45_FILTER,
    &ALL_EDGES_FILTER,
    &SHARPEN_FILTER,
    &SUBTLE_SHARPEN_FILTER,
    &EXCESSIVELY_SHARPEN_FILTER,
    NULL
};

const filter* get_filter(const char* name)
{
    for (size_t i = 0; predefined_filters[i] != NULL; ++i)
        if (strcmp(name, predefined_filters[i]->name) == 0)
            return predefined_filters[i];
    return NULL;
}

#endif
