#!/bin/bash

NUMBER_OF_CORES=`grep "core id" /proc/cpuinfo | wc -l`

for image in img/*.jpg
do
    echo $image | grep -q "\.filtered\.jpg" && continue
    echo -e "\e[0;32mProcess `basename $image`...\e[0m"
    for filter in `./filter_image -l`
    do
        echo -e "   \e[0;33mApply $filter filter...\e[0m"
        for ((i = 1; i <= $NUMBER_OF_CORES; ++i))
        do
            echo -e "       \e[0;34mRun program with $i threads...\e[0m"
            /usr/bin/time -p ./filter_image -t $i -f $filter -o "${image%.*}.$filter.filtered.jpg" "$image" 2>&1 | sed 's/^/       /'
        done
    done
done
